public class Car {

    int yopStr;
    double secsTo100Str;

    public Car(String modelName, int yop, double secsTo100) {
        this.modelName = modelName;
        this.yop = yop;
        this.secsTo100 = secsTo100;
    }    

    String modelName;
    int yop;
    double secsTo100;   
    
    @Override
    public String toString() {
        return String.format("%s from %d has %s sec", modelName, yop, secsTo100);
    } 
}