
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class NewPeopleList extends javax.swing.JFrame {

    DefaultListModel<Person> modelPersonList = new DefaultListModel<>();
    File currentFile;

    public NewPeopleList() {
        initComponents();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);

        lblNameError.setVisible(false);
        lblAgeError.setVisible(false);
        lblPostCodeError.setVisible(false);
        //
        centerOnParent(dlgEditPerson, true);
    }

    public void centerOnParent(final Window child, final boolean absolute) {
        child.pack();
        boolean useChildsOwner = child.getOwner() != null ? ((child.getOwner() instanceof JFrame) || (child.getOwner() instanceof JDialog)) : false;
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        final Dimension parentSize = useChildsOwner ? child.getOwner().getSize() : screenSize;
        final Point parentLocationOnScreen = useChildsOwner ? child.getOwner().getLocationOnScreen() : new Point(0, 0);
        final Dimension childSize = child.getSize();
        childSize.width = Math.min(childSize.width, screenSize.width);
        childSize.height = Math.min(childSize.height, screenSize.height);
        child.setSize(childSize);
        int x;
        int y;
        if ((child.getOwner() != null) && child.getOwner().isShowing()) {
            x = (parentSize.width - childSize.width) / 2;
            y = (parentSize.height - childSize.height) / 2;
            x += parentLocationOnScreen.x;
            y += parentLocationOnScreen.y;
        } else {
            x = (screenSize.width - childSize.width) / 2;
            y = (screenSize.height - childSize.height) / 2;
        }
        if (!absolute) {
            x /= 2;
            y /= 2;
        }
        child.setLocation(x, y);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popupMenu = new javax.swing.JPopupMenu();
        ppmiDeleteItem = new javax.swing.JMenuItem();
        dlgEditPerson = new javax.swing.JDialog();
        btDlgSave = new javax.swing.JButton();
        btDlgtCancel = new javax.swing.JButton();
        lblDlgNameError = new javax.swing.JLabel();
        lblDlgAgeError = new javax.swing.JLabel();
        lblDlgPostCodeError = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        tfDlgName = new javax.swing.JTextField();
        tfDlgAge = new javax.swing.JTextField();
        tfDlgPostalCode = new javax.swing.JTextField();
        fileChooser = new javax.swing.JFileChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstPeople = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btAddPerson = new javax.swing.JButton();
        tfName = new javax.swing.JTextField();
        tfAge = new javax.swing.JTextField();
        tfPostalCode = new javax.swing.JTextField();
        lblNameError = new javax.swing.JLabel();
        lblAgeError = new javax.swing.JLabel();
        lblPostCodeError = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        miOpen = new javax.swing.JMenuItem();
        miSaveAllAs = new javax.swing.JMenuItem();
        miSaveSelectedAs = new javax.swing.JMenuItem();
        miExit = new javax.swing.JMenuItem();

        ppmiDeleteItem.setText("Delete Item");
        ppmiDeleteItem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                ppmiDeleteItemMouseReleased(evt);
            }
        });
        popupMenu.add(ppmiDeleteItem);

        dlgEditPerson.setModal(true);

        btDlgSave.setText("Save changes");
        btDlgSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDlgSaveActionPerformed(evt);
            }
        });

        btDlgtCancel.setText("Cancel ");
        btDlgtCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDlgtCancelActionPerformed(evt);
            }
        });

        lblDlgNameError.setForeground(new java.awt.Color(255, 0, 51));
        lblDlgNameError.setText("Name error");

        lblDlgAgeError.setForeground(new java.awt.Color(255, 0, 51));
        lblDlgAgeError.setText("Age error");

        lblDlgPostCodeError.setForeground(new java.awt.Color(255, 0, 0));
        lblDlgPostCodeError.setText("Postal code error");

        jLabel4.setText("Name:");

        jLabel5.setText("Age:");

        jLabel6.setText("Postal Code:");

        tfDlgPostalCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfDlgPostalCodeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout dlgEditPersonLayout = new javax.swing.GroupLayout(dlgEditPerson.getContentPane());
        dlgEditPerson.getContentPane().setLayout(dlgEditPersonLayout);
        dlgEditPersonLayout.setHorizontalGroup(
            dlgEditPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgEditPersonLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(dlgEditPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dlgEditPersonLayout.createSequentialGroup()
                        .addComponent(btDlgSave)
                        .addGap(127, 127, 127)
                        .addComponent(btDlgtCancel))
                    .addGroup(dlgEditPersonLayout.createSequentialGroup()
                        .addGroup(dlgEditPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgEditPersonLayout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgEditPersonLayout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(36, 36, 36))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgEditPersonLayout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(dlgEditPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblDlgPostCodeError)
                            .addComponent(lblDlgAgeError)
                            .addComponent(lblDlgNameError)
                            .addComponent(tfDlgAge, javax.swing.GroupLayout.DEFAULT_SIZE, 266, Short.MAX_VALUE)
                            .addComponent(tfDlgName)
                            .addComponent(tfDlgPostalCode))))
                .addGap(230, 230, 230))
        );
        dlgEditPersonLayout.setVerticalGroup(
            dlgEditPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgEditPersonLayout.createSequentialGroup()
                .addGap(59, 59, 59)
                .addGroup(dlgEditPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfDlgName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDlgNameError)
                .addGap(22, 22, 22)
                .addGroup(dlgEditPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfDlgAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDlgAgeError)
                .addGap(21, 21, 21)
                .addGroup(dlgEditPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfDlgPostalCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblDlgPostCodeError)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(dlgEditPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btDlgSave)
                    .addComponent(btDlgtCancel))
                .addGap(121, 121, 121))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lstPeople.setModel(modelPersonList);
        lstPeople.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstPeopleMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lstPeopleMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(lstPeople);

        jLabel1.setText("Name:");

        jLabel2.setText("Age:");

        jLabel3.setText("Postal Code:");

        btAddPerson.setText("Add Person");
        btAddPerson.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAddPersonActionPerformed(evt);
            }
        });

        tfPostalCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfPostalCodeActionPerformed(evt);
            }
        });

        lblNameError.setForeground(new java.awt.Color(255, 0, 51));
        lblNameError.setText("Name error");

        lblAgeError.setForeground(new java.awt.Color(255, 0, 51));
        lblAgeError.setText("Age error");

        lblPostCodeError.setForeground(new java.awt.Color(255, 0, 0));
        lblPostCodeError.setText("Postal code error");

        jMenu1.setText("File");

        miOpen.setText("Open");
        miOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miOpenActionPerformed(evt);
            }
        });
        jMenu1.add(miOpen);

        miSaveAllAs.setText("Save all as");
        jMenu1.add(miSaveAllAs);

        miSaveSelectedAs.setText("Save selected as ");
        miSaveSelectedAs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSaveSelectedAsActionPerformed(evt);
            }
        });
        jMenu1.add(miSaveSelectedAs);

        miExit.setText("Exit");
        miExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miExitActionPerformed(evt);
            }
        });
        jMenu1.add(miExit);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 218, Short.MAX_VALUE)
                        .addComponent(btAddPerson, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(79, 79, 79))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(81, 81, 81)
                        .addComponent(lblPostCodeError)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblNameError)
                                    .addComponent(lblAgeError))
                                .addContainerGap(280, Short.MAX_VALUE))
                            .addComponent(tfName, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(tfAge, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(tfPostalCode)))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(1, 1, 1)
                .addComponent(lblNameError)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(1, 1, 1)
                .addComponent(lblAgeError)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfPostalCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblPostCodeError)
                .addGap(38, 38, 38)
                .addComponent(btAddPerson, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(107, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private boolean isPersonDataValid(String name, JLabel nameError, String ageStr, JLabel ageError, String postcode, JLabel postcodeError) {
        boolean isNameInvalid = (name.length() < 2 || name.length() > 20);
        lblNameError.setVisible(isNameInvalid);
        int age = 0;
        boolean isAgeInvalid;
        try {
            age = Integer.parseInt(ageStr);
            isAgeInvalid = age < 1 || age > 150;
        } catch (NumberFormatException e) {
            isAgeInvalid = true;
        }
        lblAgeError.setVisible(isAgeInvalid);
        //
        boolean isPostcodeInvalid = !postcode.matches("[A-Z][0-9][A-Z] ?[0-9][A-Z][0-9]");
        lblPostCodeError.setVisible(isPostcodeInvalid);
        //
        return (!(isNameInvalid || isAgeInvalid || isPostcodeInvalid ));
        
    }

    private void btAddPersonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAddPersonActionPerformed
        String name = tfName.getText();
        String ageStr = tfAge.getText();
        String postcode = tfPostalCode.getText().toUpperCase();
        //
        if (isPersonDataValid(name, lblNameError, ageStr, lblAgeError, postcode, lblPostCodeError)){
        //
          modelPersonList.addElement(new Person(name, Integer.parseInt(ageStr), postcode));
        }
    }//GEN-LAST:event_btAddPersonActionPerformed

    private void miOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miOpenActionPerformed
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                currentFile = fileChooser.getSelectedFile();
                String content = new Scanner(currentFile).useDelimiter("\\Z").next();
                lstPeople.add(content, jMenu1);
            } catch (IOException ex) {
                currentFile = null;
                JOptionPane.showMessageDialog(this,
                        "Unable to read file contents from " + currentFile.getAbsolutePath(),
                        "File access error",
                        JOptionPane.WARNING_MESSAGE);
                return;
            }
        }
    }//GEN-LAST:event_miOpenActionPerformed

    private void lstPeopleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstPeopleMouseClicked
        //right button clicked once
        if ((evt.getButton() == MouseEvent.BUTTON1) && (evt.getClickCount() == 2)) {
            lstPeople.setSelectedIndex(lstPeople.locationToIndex(evt.getPoint()));
            int index = lstPeople.getSelectedIndex();
            if (lstPeople.getSelectedIndex() != -1) {
                Person p = modelPersonList.get(index);
                tfDlgName.setText(p.name);
                tfDlgAge.setText(p.age + "");
                tfDlgPostalCode.setText(p.postcode);
                lblDlgNameError.setVisible(false);
                lblDlgAgeError.setVisible(false);
                lblDlgPostCodeError.setVisible(false);

                dlgEditPerson.setVisible(true);
            }

        }
    }//GEN-LAST:event_lstPeopleMouseClicked

    private void lstPeopleMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstPeopleMouseReleased
        if ((evt.getButton() == MouseEvent.BUTTON3) && (evt.getClickCount() == 1)) {
            lstPeople.setSelectedIndex(lstPeople.locationToIndex(evt.getPoint()));
            if (lstPeople.getSelectedIndex() != -1) {
                popupMenu.show(evt.getComponent(), evt.getX(), evt.getY());
            }
        }
    }//GEN-LAST:event_lstPeopleMouseReleased

    private void miSaveSelectedAsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSaveSelectedAsActionPerformed
        // TODO: show file chooser first
        int[] indexList = lstPeople.getSelectedIndices();
        for (int idx : indexList) {
            Person p = modelPersonList.get(idx);
            System.out.printf("Sel[%d]:%s\n", idx, p);
        }
    }//GEN-LAST:event_miSaveSelectedAsActionPerformed

    private void tfPostalCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfPostalCodeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfPostalCodeActionPerformed

    private void miExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miExitActionPerformed
        dispose();
    }//GEN-LAST:event_miExitActionPerformed

    private void ppmiDeleteItemMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ppmiDeleteItemMouseReleased
        int index = lstPeople.getSelectedIndex();
        Person p = modelPersonList.get(index);
        Object[] options = {"Delete", "Cancel"};
        int choice = JOptionPane.showOptionDialog(this,
                "Are you sure you want to delete item:\n"
                + p,
                "Confirm deletion",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[1]);
        if (choice == 0) {
            modelPersonList.remove(index);
        }
    }//GEN-LAST:event_ppmiDeleteItemMouseReleased

    private void tfDlgPostalCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfDlgPostalCodeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfDlgPostalCodeActionPerformed

    private void btDlgSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDlgSaveActionPerformed
        String name = tfDlgName.getText();
        String ageStr = tfDlgAge.getText();
        String postcode = tfDlgPostalCode.getText();
        if (isPersonDataValid(name, lblDlgNameError, ageStr, lblDlgAgeError, postcode, lblDlgPostCodeError )){
            Person p = modelPersonList.get(lstPeople.getSelectedIndex());
            //save changes
            p.name = name;
            p.age =Integer.parseInt(ageStr);
            p.postcode = postcode;
            //
            dlgEditPerson.setVisible(false);
        }
        
    }//GEN-LAST:event_btDlgSaveActionPerformed

    private void btDlgtCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDlgtCancelActionPerformed
        dlgEditPerson.setVisible(false);
    }//GEN-LAST:event_btDlgtCancelActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NewPeopleList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NewPeopleList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NewPeopleList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NewPeopleList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NewPeopleList().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAddPerson;
    private javax.swing.JButton btDlgSave;
    private javax.swing.JButton btDlgtCancel;
    private javax.swing.JDialog dlgEditPerson;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblAgeError;
    private javax.swing.JLabel lblDlgAgeError;
    private javax.swing.JLabel lblDlgNameError;
    private javax.swing.JLabel lblDlgPostCodeError;
    private javax.swing.JLabel lblNameError;
    private javax.swing.JLabel lblPostCodeError;
    private javax.swing.JList<Person> lstPeople;
    private javax.swing.JMenuItem miExit;
    private javax.swing.JMenuItem miOpen;
    private javax.swing.JMenuItem miSaveAllAs;
    private javax.swing.JMenuItem miSaveSelectedAs;
    private javax.swing.JPopupMenu popupMenu;
    private javax.swing.JMenuItem ppmiDeleteItem;
    private javax.swing.JTextField tfAge;
    private javax.swing.JTextField tfDlgAge;
    private javax.swing.JTextField tfDlgName;
    private javax.swing.JTextField tfDlgPostalCode;
    private javax.swing.JTextField tfName;
    private javax.swing.JTextField tfPostalCode;
    // End of variables declaration//GEN-END:variables
}
