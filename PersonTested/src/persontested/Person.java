
package persontested;

import java.util.Date;


public class Person {
    
    public Person(String name, int age, String postalCode, double weightKg, Date dateOfBirth) {
        setName(name);
        setAge(age);
        setPostalCode(postalCode) ;
        setWeightKg(weightKg);
        setDateOfBirth(dateOfBirth);
        
    }
    
    public Person(String name, int age) {
        setName(name);
        setAge(age);
                
    }
    
    private String name;
    private int age;
    // Homework : 1) encapsulate fields, 2) write setters, 3) write unit tests for each setter
    private String postalCode; //"7A7 A7A"
    private double weightKg; // 0-300 (both inclusive)
    private Date dateOfBirth; // valid date YYYY-MM-DD from 1900 to 2099 year
    
    
    public int getAge() {
        return age;
    }

    public final void setAge(int age) {
        if (age < 0  || age > 150 ){
            throw new IllegalArgumentException("Invalid age");
        }
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public final void setName(String name) {
        if ( name == null || name.length() < 2 ){
            throw new IllegalArgumentException("Name too short");
        }
        this.name = name;
    }    

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        if (!postalCode.toUpperCase().matches("[A-Z][0-9][A-Z] ?[0-9][A-Z][0-9]")){
            throw new IllegalArgumentException("Invalid postal code");
        }
        this.postalCode = postalCode;
    }

    public double getWeightKg() {
        return weightKg;
    }

    public void setWeightKg(double weightKg) {
        this.weightKg = weightKg;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    
    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", age=" + age + ", postalCode=" + postalCode + ", weightKg=" + weightKg + ", dateOfBirth=" + dateOfBirth + '}';
    }
}
