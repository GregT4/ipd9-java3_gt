
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;


public class Quiz1IceCreamSelector extends javax.swing.JFrame {

    File currentFile;
    boolean isModified; 
    String[] toSplit;
    
    public Quiz1IceCreamSelector() {
        initComponents();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem3 = new javax.swing.JMenuItem();
        bgScoopsize = new javax.swing.ButtonGroup();
        fileChooser = new javax.swing.JFileChooser();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        tfName = new javax.swing.JTextField();
        rbSmall = new javax.swing.JRadioButton();
        rbMedium = new javax.swing.JRadioButton();
        rbLarge = new javax.swing.JRadioButton();
        cbChocolate = new javax.swing.JCheckBox();
        cbSprinkles = new javax.swing.JCheckBox();
        comboConeType = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        textArea = new javax.swing.JTextArea();
        btFlavour1 = new javax.swing.JButton();
        btFlavour2 = new javax.swing.JButton();
        btFlavour3 = new javax.swing.JButton();
        btFlavour4 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        miLoadFlavors = new javax.swing.JMenuItem();
        miSaveOrderAs = new javax.swing.JMenuItem();
        miExit = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();

        jMenuItem3.setText("jMenuItem3");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Customer name:");

        jLabel2.setText("Toppings:");

        jLabel3.setText("Scoop size:");

        jLabel4.setText("Cone type:");

        tfName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfNameActionPerformed(evt);
            }
        });

        bgScoopsize.add(rbSmall);
        rbSmall.setText("small");
        rbSmall.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbSmallActionPerformed(evt);
            }
        });

        bgScoopsize.add(rbMedium);
        rbMedium.setText("medium");

        bgScoopsize.add(rbLarge);
        rbLarge.setText("large");

        cbChocolate.setText("chocolate");

        cbSprinkles.setText("sprinkles");

        comboConeType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Waffle", "Sugar", "Chocolate" }));
        comboConeType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboConeTypeActionPerformed(evt);
            }
        });

        jLabel5.setText("Orders");

        textArea.setEditable(false);
        textArea.setColumns(20);
        textArea.setRows(5);
        jScrollPane1.setViewportView(textArea);

        btFlavour1.setText("add Flavour1");
        btFlavour1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFlavour1ActionPerformed(evt);
            }
        });

        btFlavour2.setText("add Flavour2");
        btFlavour2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFlavour2ActionPerformed(evt);
            }
        });

        btFlavour3.setText("add Flavour3");
        btFlavour3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFlavour3ActionPerformed(evt);
            }
        });

        btFlavour4.setText("add Flavour4");
        btFlavour4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFlavour4ActionPerformed(evt);
            }
        });

        jMenu1.setText("File");

        miLoadFlavors.setText("Load Flavors");
        miLoadFlavors.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miLoadFlavorsActionPerformed(evt);
            }
        });
        jMenu1.add(miLoadFlavors);

        miSaveOrderAs.setText("Save Order As");
        miSaveOrderAs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSaveOrderAsActionPerformed(evt);
            }
        });
        jMenu1.add(miSaveOrderAs);

        miExit.setText("Exit");
        jMenu1.add(miExit);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btFlavour1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btFlavour2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btFlavour3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btFlavour4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(4, 4, 4))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(comboConeType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(rbSmall)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(rbMedium)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(rbLarge))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(cbChocolate)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(cbSprinkles)))))
                        .addGap(18, 18, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 333, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(rbSmall)
                            .addComponent(rbMedium)
                            .addComponent(rbLarge))
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(cbChocolate)
                            .addComponent(cbSprinkles))
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(comboConeType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(33, 33, 33)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btFlavour2, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btFlavour1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btFlavour3, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btFlavour4, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void comboConeTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboConeTypeActionPerformed
        String conetype = (String) comboConeType.getSelectedItem();
    }//GEN-LAST:event_comboConeTypeActionPerformed

    private void rbSmallActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbSmallActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbSmallActionPerformed

    private void btFlavour2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFlavour2ActionPerformed
        String ScoopSize;
        if (tfName.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this,
                    "You must enter a customer name!",
                    "Customer name missing",
                    JOptionPane.WARNING_MESSAGE);
            return;
        } else {
            if (rbSmall.isSelected()) {
                ScoopSize = "Small";
            } else if (rbMedium.isSelected()) {
                ScoopSize = "Medium";
            } else if (rbLarge.isSelected()) {
                ScoopSize = "Large";
            } else {
                JOptionPane.showMessageDialog(this,
                        "Unknown size selection",
                        "Selection error",
                        JOptionPane.WARNING_MESSAGE);
                return;
            }
        }
        ArrayList<String> Topping = new ArrayList<>();
        if (cbChocolate.isSelected()) {
            Topping.add("Chocolate");
        }
        if (cbSprinkles.isSelected()) {
            Topping.add("Sprinkles");
        }
        String ToppingList = "";
        for (String t : Topping) {
            if (ToppingList.equals("")) {
                ToppingList = t;
            } else {
                ToppingList += "," + t;
            }
        }
        String record = String.format("%s;%s;%s;%s;%s\n",
                tfName.getText(), ScoopSize, ToppingList, comboConeType.getSelectedItem().toString(), toSplit[0]);
        textArea.append(record);
        isModified = true;
    }//GEN-LAST:event_btFlavour2ActionPerformed

    private void btFlavour1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFlavour1ActionPerformed
        String ScoopSize;
        if (tfName.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this,
                    "You must enter a customer name!",
                    "Customer name missing",
                    JOptionPane.WARNING_MESSAGE);
            return;
        } else {
            if (rbSmall.isSelected()) {
                ScoopSize = "Small";
            } else if (rbMedium.isSelected()) {
                ScoopSize = "Medium";
            } else if (rbLarge.isSelected()) {
                ScoopSize = "Large";
            } else {
                JOptionPane.showMessageDialog(this,
                        "Unknown size selection",
                        "Selection error",
                        JOptionPane.WARNING_MESSAGE);
                return;
            }
        }
        ArrayList<String> Topping = new ArrayList<>();
        if (cbChocolate.isSelected()) {
            Topping.add("Chocolate");
        }
        if (cbSprinkles.isSelected()) {
            Topping.add("Sprinkles");
        }
        String ToppingList = "";
        for (String t : Topping) {
            if (ToppingList.equals("")) {
                ToppingList = t;
            } else {
                ToppingList += "," + t;
            }
        }
        String record = String.format("%s;%s;%s;%s;%s\n",
                tfName.getText(), ScoopSize, ToppingList, comboConeType.getSelectedItem().toString(), toSplit[0]);
        textArea.append(record);
        isModified = true;
    }//GEN-LAST:event_btFlavour1ActionPerformed

    private void btFlavour3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFlavour3ActionPerformed
        String ScoopSize;
        if (tfName.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this,
                    "You must enter a customer name!",
                    "Customer name missing",
                    JOptionPane.WARNING_MESSAGE);
            return;
        } else {
            if (rbSmall.isSelected()) {
                ScoopSize = "Small";
            } else if (rbMedium.isSelected()) {
                ScoopSize = "Medium";
            } else if (rbLarge.isSelected()) {
                ScoopSize = "Large";
            } else {
                JOptionPane.showMessageDialog(this,
                        "Unknown size selection",
                        "Selection error",
                        JOptionPane.WARNING_MESSAGE);
                return;
            }
        }
        ArrayList<String> Topping = new ArrayList<>();
        if (cbChocolate.isSelected()) {
            Topping.add("Chocolate");
        }
        if (cbSprinkles.isSelected()) {
            Topping.add("Sprinkles");
        }
        String ToppingList = "";
        for (String t : Topping) {
            if (ToppingList.equals("")) {
                ToppingList = t;
            } else {
                ToppingList += "," + t;
            }
        }
        String record = String.format("%s;%s;%s;%s;%s\n",
                tfName.getText(), ScoopSize, ToppingList, comboConeType.getSelectedItem().toString(), toSplit[0]);
        textArea.append(record);
        isModified = true;
    }//GEN-LAST:event_btFlavour3ActionPerformed

    private void btFlavour4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFlavour4ActionPerformed
        String ScoopSize;
        if (tfName.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this,
                    "You must enter a customer name!",
                    "Customer name missing",
                    JOptionPane.WARNING_MESSAGE);
            return;
        } else {
            if (rbSmall.isSelected()) {
                ScoopSize = "Small";
            } else if (rbMedium.isSelected()) {
                ScoopSize = "Medium";
            } else if (rbLarge.isSelected()) {
                ScoopSize = "Large";
            } else {
                JOptionPane.showMessageDialog(this,
                        "Unknown size selection",
                        "Selection error",
                        JOptionPane.WARNING_MESSAGE);
                return;
            }
        }
        ArrayList<String> Topping = new ArrayList<>();
        if (cbChocolate.isSelected()) {
            Topping.add("Chocolate");
        }
        if (cbSprinkles.isSelected()) {
            Topping.add("Sprinkles");
        }
        String ToppingList = "";
        for (String t : Topping) {
            if (ToppingList.equals("")) {
                ToppingList = t;
            } else {
                ToppingList += "," + t;
            }
        }
        String record = String.format("%s;%s;%s;%s;%s\n",
                tfName.getText(), ScoopSize, ToppingList, comboConeType.getSelectedItem().toString(), toSplit[0]);
        textArea.append(record);
        isModified = true;
    }//GEN-LAST:event_btFlavour4ActionPerformed

    private void miLoadFlavorsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miLoadFlavorsActionPerformed
         if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                currentFile = fileChooser.getSelectedFile();
                String content = new Scanner(currentFile).useDelimiter("\\Z").next();
                toSplit = content.split("\\n");
                for (int i = 0; i < 4; ++i) {
                    switch (i) {
                        case 0:
                            btFlavour1.setText("Add " + toSplit[i]);
                            btFlavour1.setEnabled(true);
                            break;
                        case 1:
                            btFlavour2.setText("Add " + toSplit[i]);
                            btFlavour2.setEnabled(true);
                            break;
                        case 2:
                            btFlavour3.setText("Add " + toSplit[i]);
                            btFlavour3.setEnabled(true);
                            break;
                        case 3:
                            btFlavour4.setText("Add " + toSplit[i]);
                            btFlavour4.setEnabled(true);
                            break;
                        default:
                            break;
                    }
                }
            } catch (IOException ex) {
                currentFile = null;
                JOptionPane.showMessageDialog(this,
                        "Unable to read file contents from " + currentFile.getAbsolutePath(),
                        "File access error",
                        JOptionPane.WARNING_MESSAGE);
                return;
            }
        }
    }//GEN-LAST:event_miLoadFlavorsActionPerformed

    private void tfNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfNameActionPerformed
        String name = tfName.getText();
        if (name.isEmpty()) {
            JOptionPane.showMessageDialog(this,
                "You forgot to enter your name.",
                "Name missing",
                JOptionPane.WARNING_MESSAGE);
            return;
        }
    }//GEN-LAST:event_tfNameActionPerformed

    private void miSaveOrderAsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSaveOrderAsActionPerformed
        if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            currentFile = fileChooser.getSelectedFile();
            // FIXME: what if user selected "All files" filter?

            if (!currentFile.getName().toLowerCase().endsWith(".ord")) {
                currentFile = new File(currentFile.getAbsolutePath() + ".ord");
            }
            try (PrintWriter pw = new PrintWriter(currentFile)) {
                pw.write(textArea.getText());
                isModified = false;
                textArea.removeAll();
                textArea.setText("");
            } catch (IOException e) {
                JOptionPane.showMessageDialog(this,
                        "Unable to write file contents to " + currentFile.getAbsolutePath(),
                        "File access error",
                        JOptionPane.WARNING_MESSAGE);

            }
        }
    }//GEN-LAST:event_miSaveOrderAsActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Quiz1IceCreamSelector.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Quiz1IceCreamSelector.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Quiz1IceCreamSelector.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Quiz1IceCreamSelector.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Quiz1IceCreamSelector().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgScoopsize;
    private javax.swing.JButton btFlavour1;
    private javax.swing.JButton btFlavour2;
    private javax.swing.JButton btFlavour3;
    private javax.swing.JButton btFlavour4;
    private javax.swing.JCheckBox cbChocolate;
    private javax.swing.JCheckBox cbSprinkles;
    private javax.swing.JComboBox<String> comboConeType;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenuItem miExit;
    private javax.swing.JMenuItem miLoadFlavors;
    private javax.swing.JMenuItem miSaveOrderAs;
    private javax.swing.JRadioButton rbLarge;
    private javax.swing.JRadioButton rbMedium;
    private javax.swing.JRadioButton rbSmall;
    private javax.swing.JTextArea textArea;
    private javax.swing.JTextField tfName;
    // End of variables declaration//GEN-END:variables
}
