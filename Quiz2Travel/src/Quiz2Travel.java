
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.MouseEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Quiz2Travel extends javax.swing.JFrame {
    
    DefaultListModel<Trip> modelTripList = new DefaultListModel<>();
    Date depatrureDate, returnDate;
    Trip t;
    File currentFile;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/mm/dd");

    public Quiz2Travel() {
        initComponents();
        //center parent jframe
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        //hide dlg errors
        lblDlgDestinationError.setVisible(false);
        lblDlgTravellerNameError.setVisible(false);
        lblDlgTravellerPassportError.setVisible(false);
        lblDlgDepartureDateError.setVisible(false);
        lblDlgReturnDateError.setVisible(false);
        //center Dialogbox to parent
        centerOnParent(dlgAddTrip, true);
    }

    public void centerOnParent(final Window child, final boolean absolute) {
        child.pack();
        boolean useChildsOwner = child.getOwner() != null ? ((child.getOwner() instanceof JFrame) || (child.getOwner() instanceof JDialog)) : false;
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        final Dimension parentSize = useChildsOwner ? child.getOwner().getSize() : screenSize;
        final Point parentLocationOnScreen = useChildsOwner ? child.getOwner().getLocationOnScreen() : new Point(0, 0);
        final Dimension childSize = child.getSize();
        childSize.width = Math.min(childSize.width, screenSize.width);
        childSize.height = Math.min(childSize.height, screenSize.height);
        child.setSize(childSize);
        int x;
        int y;
        if ((child.getOwner() != null) && child.getOwner().isShowing()) {
            x = (parentSize.width - childSize.width) / 2;
            y = (parentSize.height - childSize.height) / 2;
            x += parentLocationOnScreen.x;
            y += parentLocationOnScreen.y;
        } else {
            x = (screenSize.width - childSize.width) / 2;
            y = (screenSize.height - childSize.height) / 2;
        }
        if (!absolute) {
            x /= 2;
            y /= 2;
        }
        child.setLocation(x, y);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dlgAddTrip = new javax.swing.JDialog();
        btDlgAddTrip = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        tfDlgDestination = new javax.swing.JTextField();
        tfDlgTravellerName = new javax.swing.JTextField();
        tfDlgTravellerPassportNo = new javax.swing.JTextField();
        tfDlgDepartureDate = new javax.swing.JTextField();
        tfDlgReturnDate = new javax.swing.JTextField();
        lblDlgDestinationError = new javax.swing.JLabel();
        lblDlgTravellerNameError = new javax.swing.JLabel();
        lblDlgTravellerPassportError = new javax.swing.JLabel();
        lblDlgDepartureDateError = new javax.swing.JLabel();
        lblDlgReturnDateError = new javax.swing.JLabel();
        popupMenu = new javax.swing.JPopupMenu();
        btSaveSelected = new javax.swing.JButton();
        btAddTrip = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstTrip = new javax.swing.JList<>();

        dlgAddTrip.setModal(true);

        btDlgAddTrip.setText("Add Trip");
        btDlgAddTrip.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDlgAddTripActionPerformed(evt);
            }
        });

        jLabel1.setText("Destination:");

        jLabel2.setText("Name:");

        jLabel3.setText("Passport:");

        jLabel4.setText("Departure:");

        jLabel5.setText("Return:");

        tfDlgDestination.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfDlgDestinationActionPerformed(evt);
            }
        });

        tfDlgDepartureDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfDlgDepartureDateActionPerformed(evt);
            }
        });

        lblDlgDestinationError.setForeground(new java.awt.Color(255, 51, 51));
        lblDlgDestinationError.setText("Destination error");

        lblDlgTravellerNameError.setForeground(new java.awt.Color(255, 0, 0));
        lblDlgTravellerNameError.setText("Name error");

        lblDlgTravellerPassportError.setForeground(new java.awt.Color(255, 0, 0));
        lblDlgTravellerPassportError.setText("Passport error");

        lblDlgDepartureDateError.setForeground(new java.awt.Color(255, 0, 0));
        lblDlgDepartureDateError.setText("Departure error");

        lblDlgReturnDateError.setForeground(new java.awt.Color(255, 0, 0));
        lblDlgReturnDateError.setText("Return error");

        javax.swing.GroupLayout dlgAddTripLayout = new javax.swing.GroupLayout(dlgAddTrip.getContentPane());
        dlgAddTrip.getContentPane().setLayout(dlgAddTripLayout);
        dlgAddTripLayout.setHorizontalGroup(
            dlgAddTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgAddTripLayout.createSequentialGroup()
                .addGroup(dlgAddTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dlgAddTripLayout.createSequentialGroup()
                        .addGap(159, 159, 159)
                        .addComponent(btDlgAddTrip))
                    .addGroup(dlgAddTripLayout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addGroup(dlgAddTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(dlgAddTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tfDlgDestination)
                            .addComponent(tfDlgTravellerName)
                            .addComponent(tfDlgTravellerPassportNo)
                            .addComponent(tfDlgDepartureDate)
                            .addComponent(tfDlgReturnDate)
                            .addComponent(lblDlgDestinationError, javax.swing.GroupLayout.DEFAULT_SIZE, 262, Short.MAX_VALUE)
                            .addComponent(lblDlgReturnDateError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblDlgDepartureDateError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblDlgTravellerPassportError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblDlgTravellerNameError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(30, Short.MAX_VALUE))
        );
        dlgAddTripLayout.setVerticalGroup(
            dlgAddTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgAddTripLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(dlgAddTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(tfDlgDestination, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDlgDestinationError)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addGroup(dlgAddTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(tfDlgTravellerName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDlgTravellerNameError)
                .addGap(24, 24, 24)
                .addGroup(dlgAddTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(tfDlgTravellerPassportNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDlgTravellerPassportError)
                .addGap(25, 25, 25)
                .addGroup(dlgAddTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(tfDlgDepartureDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDlgDepartureDateError)
                .addGap(18, 18, 18)
                .addGroup(dlgAddTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(tfDlgReturnDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDlgReturnDateError)
                .addGap(18, 18, 18)
                .addComponent(btDlgAddTrip)
                .addGap(39, 39, 39))
        );

        popupMenu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                popupMenuMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                popupMenuMouseReleased(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btSaveSelected.setText("Save Selected");
        btSaveSelected.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSaveSelectedActionPerformed(evt);
            }
        });

        btAddTrip.setText("Add Trip");

        lstTrip.setModel(modelTripList);
        lstTrip.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstTripMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(lstTrip);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btSaveSelected, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(92, 92, 92)
                        .addComponent(btAddTrip, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btSaveSelected)
                    .addComponent(btAddTrip))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 237, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tfDlgDestinationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfDlgDestinationActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfDlgDestinationActionPerformed

    private void tfDlgDepartureDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfDlgDepartureDateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfDlgDepartureDateActionPerformed

    private boolean isTripDataValid(String destination, lblDlgDestinationError,String travellerName, lblDlgTravellerNameError, String travellerPassportNo, lblDlgTravellerPassportError,
                                     String departureDate, lblDlgDepartureDateError,String returnDate, lblDlgReturnDateError) {
        boolean isDestinationInvalid = (destination.length() < 2 || destination.length() > 20);
        lblDlgDestinationError.setVisible(isDestinationInvalid);
        //
        boolean isNameInvalid = (travellerName.length() < 2 || travellerName.length() > 20);
        lblDlgTravellerNameError.setVisible(isNameInvalid);
        //
        boolean isTravellerPassportNoInvalid = !travellerPassportNo.matches("[A-PR-WYa-pr-wy][1-9]\\d\\s?\\d{4}[1-9]");
        lblDlgTravellerPassportError.setVisible(isTravellerPassportNoInvalid);
        //
        return (!(isDestinationInvalid || isNameInvalid || isTravellerPassportNoInvalid ));
        
    }
    
    private void btDlgAddTripActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDlgAddTripActionPerformed
        String destination = tfDlgDestination.getText();
        String travellerName = tfDlgTravellerName.getText();
        String travellerPassportNo = tfDlgTravellerPassportNo.getText();
        String departureDate = tfDlgDepartureDate.getText();
        //
        if (isTripDataValid(travellerName, lblDlgTravellerNameError, travellerPassportNo, lblDlgTravellerPassportError, destination, lblDlgDestinationError, departureDate, lblDlgDepartureDateError)) {
            //
            modelTripList.addElement(new Trip(travellerName, travellerPassportNo, destination, departureDate));
        }
    }//GEN-LAST:event_btDlgAddTripActionPerformed

    private void btSaveSelectedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSaveSelectedActionPerformed
        int[] indexList = lstTrip.getSelectedIndices();
        for (int idx : indexList) {
            Trip p = modelTripList.get(idx);
            System.out.printf("%s\n", idx, p);
        }
    }//GEN-LAST:event_btSaveSelectedActionPerformed

    private void lstTripMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstTripMouseClicked
       if ((evt.getButton() == MouseEvent.BUTTON1) && (evt.getClickCount() == 2)) {
            lstTrip.setSelectedIndex(lstTrip.locationToIndex(evt.getPoint()));
            int index = lstTrip.getSelectedIndex();
            if (lstTrip.getSelectedIndex() != -1) {
                Trip t = modelTripList.get(index);
                tfDlgTravellerName.setText(t.travellerName);
                tfDlgTravellerPassportNo.setText(t.travellerPassportNo);
                
                lblDlgTravellerNameError.setVisible(false);
                lblDlgDestinationError.setVisible(false);
               

                dlgAddTrip.setVisible(true);
            }

        }
    }//GEN-LAST:event_lstTripMouseClicked

    private void popupMenuMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_popupMenuMouseReleased
       if ((evt.getButton() == MouseEvent.BUTTON3) && (evt.getClickCount() == 1)) {
            lstTrip.setSelectedIndex(lstTrip.locationToIndex(evt.getPoint()));
            if (lstTrip.getSelectedIndex() != -1) {
                popupMenu.show(evt.getComponent(), evt.getX(), evt.getY());
            }
        } 
    }//GEN-LAST:event_popupMenuMouseReleased

    private void popupMenuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_popupMenuMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_popupMenuMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Quiz2Travel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Quiz2Travel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Quiz2Travel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Quiz2Travel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Quiz2Travel().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAddTrip;
    private javax.swing.JButton btDlgAddTrip;
    private javax.swing.JButton btSaveSelected;
    private javax.swing.JDialog dlgAddTrip;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblDlgDepartureDateError;
    private javax.swing.JLabel lblDlgDestinationError;
    private javax.swing.JLabel lblDlgReturnDateError;
    private javax.swing.JLabel lblDlgTravellerNameError;
    private javax.swing.JLabel lblDlgTravellerPassportError;
    private javax.swing.JList<Trip> lstTrip;
    private javax.swing.JPopupMenu popupMenu;
    private javax.swing.JTextField tfDlgDepartureDate;
    private javax.swing.JTextField tfDlgDestination;
    private javax.swing.JTextField tfDlgReturnDate;
    private javax.swing.JTextField tfDlgTravellerName;
    private javax.swing.JTextField tfDlgTravellerPassportNo;
    // End of variables declaration//GEN-END:variables

    private boolean isTripDataValid(String travellerName, JLabel lblDlgTravellerNameError, String travellerPassportNo, JLabel lblDlgTravellerPassportError, String destination, JLabel lblDlgDestinationError, String departureDate, JLabel lblDlgDepartureDateError) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
