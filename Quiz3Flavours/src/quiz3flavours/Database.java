
package quiz3flavours;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import quiz3flavours.entity.Flavour;


public class Database {
    private Connection conn;
    public Database()throws SQLException {
        conn = DriverManager.getConnection("jdbc:sqlite:quiz3flavours.sqlite");
    }
    
    public ArrayList<Flavour> getAllFlavs() throws SQLException {
        final String SELECT_ALL_REPS = "SELECT * FROM representative";
        ArrayList<Flavour> result = new ArrayList<>();
        try (Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_ALL_REPS);
            while (rs.next()) {
                int id = rs.getInt("flavourID");
                String name = rs.getString("name");
                Flavour f = new Flavour(id, name);
                result.add(f);
            }
        }
        return result;
    }
    
    public void addFlav(Flavour flav) throws SQLException {
        String query = "INSERT INTO flavour VALUES (NULL, ?)";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, flav.name);
        ps.execute();
    }
}
