

package quiz3flavours.entity;


public class Flavour {
    
    public Flavour(int flavourID, String name) {
        setFlavourID(flavourID);
        setName(name);
    }
    
    private int flavourID;
    private String name;
    
    public int getFlavourID() {
        return flavourID;
    }

    public void setFlavourID(int flaovourID) {
        this.flavourID = flaovourID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null || name.length() < 2) {
            throw new IllegalArgumentException("Name too short");
        }
        this.name = name;
    }
 
    @Override
    public String toString() {
        return "MainWindow{" + "flaovourID=" + flavourID + ", name=" + name + '}';
    }
}
