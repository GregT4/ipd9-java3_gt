
package quiz3flavours.entity;


public class PlacedOrder {
    
    public PlacedOrder(int placeOrderID, String customerName, String flavourList) {
       setPlaceOrderID(placeOrderID);
       setCustomerName(customerName);
       setFlavourList(flavourList);
    }
    
    private int placeOrderID;
    private String customerName, flavourList;
    
    public int getPlaceOrderID() {
        return placeOrderID;
    }

    public void setPlaceOrderID(int placeOrderID) {
        this.placeOrderID = placeOrderID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
         if (customerName == null || customerName.length() < 2) {
            throw new IllegalArgumentException("Name too short");
        }
        this.customerName = customerName;        
    }

    public String getFlavourList() {
        return flavourList;
    }

    public void setFlavourList(String flavourList) {
        if (flavourList == "" || flavourList == null) {
            throw new IllegalArgumentException("Name too short");
        }
        this.flavourList = flavourList;
    }
    

   

    @Override
    public String toString() {
        return "PlaceOrder{" + "placeOrderID=" + placeOrderID + ", customerName=" + customerName + ", flavourList=" + flavourList + '}';
    }
  
}
