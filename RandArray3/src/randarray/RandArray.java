
package randarray;

import java.util.Random;
import java.util.Scanner;


public class RandArray {

    
    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        System.out.println("How many rows?"); 
        int rowCount = input.nextInt();  
        System.out.println("How many columns?"); 
        int colCount = input.nextInt();
        System.out.println("Minimum");
        int minRand = input.nextInt();
        System.out.println("Maximum");
        int maxRand = input.nextInt();
        Random r = new Random();
        
        int randArray[][] = new int[rowCount][colCount];         
        for (int row = 0; row < rowCount; row++){
          for (int col = 0; col < colCount; col++){
               
             randArray[row][col] = r.nextInt(maxRand-minRand+1) + minRand;
          }
        }
        for (int row = 0; row < rowCount; row++){
          for (int col = 0; col < colCount; col++){
              System.out.printf("%3d, ",randArray[row][col]);
          }
            System.out.println();
        }
    }
    
}
