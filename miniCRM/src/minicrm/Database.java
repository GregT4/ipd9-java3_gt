
package minicrm;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import minicrm.entity.Customer;
import minicrm.entity.Representative;


public class Database {
    private Connection conn;
    public Database()throws SQLException {
        conn = DriverManager.getConnection("jdbc:sqlite:minicrm.db");
    }
    
    public ArrayList<Representative> getAllReps() throws SQLException {
        final String SELECT_ALL_REPS = "SELECT * FROM representative";
        ArrayList<Representative> result = new ArrayList<>();
        try (Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_ALL_REPS);
            while (rs.next()) {
                int id = rs.getInt("representativeID");
                String nameFirst = rs.getString("nameFirst");
                String nameLast = rs.getString("nameLast");
                Representative r = new Representative(id, nameFirst, nameLast);
                result.add(r);
            }
        }
        return result;
    }
    
    public void addRep(Representative rep) throws SQLException {
        String query = "INSERT INTO representative VALUES (NULL, ?, ?)";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, rep.nameFirst);
        ps.setString(2, rep.nameLast);
        ps.execute();
    }
    
    public ArrayList<Customer> getAllCustomers() throws SQLException {
        final String SELECT_ALL_CUST = "SELECT * FROM customer";
        ArrayList<Customer> result = new ArrayList<>();
        try (Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_ALL_CUST);
            while (rs.next()) {
                int id = rs.getInt("customerID");
                String nameFirst = rs.getString("nameFirst");
                String nameLast = rs.getString("nameLast");
                String address = rs.getString("address");
                String postalCode = rs.getString("postalCode");
                String phoneNumber = rs.getString("phoneNumber");
                String SIN = rs.getString("SIN");
                Customer r = new Customer(id, nameFirst, nameLast, address, postalCode, phoneNumber, SIN);
                result.add(r);
            }
        }
        return result;
    }
    
    public void addCustomer(Customer cust) throws SQLException {
        String query = "INSERT INTO customer VALUES (NULL, ?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, cust.nameFirst);
        ps.setString(2, cust.nameLast);
        ps.setString(3, address);
        ps.setString(4, postalCode);
        ps.setString(5, phoneNumber);
        ps.setString(6, SIN);
        ps.execute();
    }
}
