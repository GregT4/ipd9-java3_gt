package minicrm.entity;

public class Customer {

    public int customerID;
    public String nameFirst, nameLast, address, postalCode, phoneNumber, SIN;

    public Customer(int customerID, String nameFirst, String nameLast, String address, String postalCode, String phoneNumber, String SIN) {
        this.customerID = customerID;
        this.nameFirst = nameFirst;
        this.nameLast = nameLast;
        this.address = address;
        this.postalCode = postalCode;
        this.phoneNumber = phoneNumber;
        this.SIN = SIN;
    }

    @Override
    public String toString() {
        return "Customer{" + "customerID=" + customerID + ", nameFirst=" + nameFirst + ", nameLast=" + nameLast + ", address=" + address + ", postalCode=" + postalCode + ", phoneNumber=" + phoneNumber + ", SIN=" + SIN + '}';
    }

}
