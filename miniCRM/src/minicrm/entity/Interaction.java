package minicrm.entity;

public class Interaction {

    public int interactionID, customerID, repID;
    public String date, notes;

    @Override
    public String toString() {
        return "Interaction{" + "interactionID=" + interactionID + ", customerID=" + customerID + ", repID=" + repID + ", date=" + date + ", notes=" + notes + '}';
    }

    public Interaction(int interactionID, int repID, String date, String notes) {
        this.interactionID = interactionID;
        this.repID = repID;
        this.date = date;
        this.notes = notes;
    }
}
