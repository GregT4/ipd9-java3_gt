package minicrm.entity;

public class Representative {

    public int representativeID;
    public String nameFirst, nameLast;

    @Override
    public String toString() {
        return "Representative{" + "representativeID=" + representativeID + ", nameFirst=" + nameFirst + ", nameLast=" + nameLast + '}';
    }

    public Representative(int representativeID, String nameFirst, String nameLast) {
        this.representativeID = representativeID;
        this.nameFirst = nameFirst;
        this.nameLast = nameLast;
    }
}
